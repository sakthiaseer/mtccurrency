﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using PX.Data;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace MTCCurrency
{
    public class NovatoService
    {

        public async Task<Dictionary<string, decimal>> GetApiData(ApiParam param)
        {
            try
            {
                var url = param.APIUrl;
                var client = new RestClient(url)
                {
                    Timeout = -1
                };
                var clientRequest = new RestRequest(Method.GET);
                if (param.AuthType == "AuthCredential")
                {
                    client.Authenticator = new RestSharp.Authenticators.HttpBasicAuthenticator(param.UserName, param.Password);
                }
                if (param.AuthType == "AuthToken")
                {
                    clientRequest.AddHeader("Authorization", param.AuthToken);
                }
                if (param.AuthType == "Accesskey")
                {
                    clientRequest.AddHeader("access_key", param.AcceptHeader);
                }
                if (!string.IsNullOrEmpty(param.AcceptHeader))
                {
                    clientRequest.AddHeader("Accept", param.AcceptHeader);
                }
                clientRequest.AddHeader("Content-Type", param.ContentType);
                IRestResponse response = await client.ExecuteTaskAsync(clientRequest);
                return LoadApiContent(param, response.Content.ToString());
            }
            catch (Exception ex)
            {
                PXTrace.WriteError(ex);
                throw ex;
            }

        }

        public async Task<string> GetApiContent(ApiParam param)
        {
            try
            {
                var url = param.APIUrl;
                var client = new RestClient(url)
                {
                    Timeout = -1
                };
                var clientRequest = new RestRequest(Method.GET);
                if (param.AuthType == "AuthCredential")
                {
                    client.Authenticator = new RestSharp.Authenticators.HttpBasicAuthenticator(param.UserName, param.Password);
                }
                if (param.AuthType == "AuthToken")
                {
                    clientRequest.AddHeader("Authorization", param.AuthToken);
                }
                if (!string.IsNullOrEmpty(param.AcceptHeader))
                {
                    clientRequest.AddHeader("Accept", param.AcceptHeader);
                }
                clientRequest.AddHeader("Content-Type", param.ContentType);


                IRestResponse response = await client.ExecuteTaskAsync(clientRequest);
                return response.Content.ToString();
            }
            catch (Exception ex)
            {
                PXTrace.WriteError(ex);
                throw ex;
            }

        }

        private Dictionary<string, decimal> LoadApiContent(ApiParam apiparam, string resultContent)
        {
            try
            {
                Dictionary<string, decimal> apiPairs = new Dictionary<string, decimal>();
                if (apiparam.ResponseType == "XML")
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(resultContent);
                    resultContent = JsonConvert.SerializeXmlNode(doc);
                }

                var myJObject = JObject.Parse(resultContent);
                var myList = myJObject.SelectTokens(apiparam.RootNode).Values<dynamic>().ToList();
                if (myList.Count == 0)
                {
                    throw new PXException("Please use valid API.");
                }
                for (int i = 0; i < myList.Count; i++)
                {
                    var curryCode = "";
                    decimal? curryRate = null;
                    if (!string.IsNullOrEmpty(apiparam.CurrencyNode) && !string.IsNullOrEmpty(apiparam.RatioNode))
                    {
                        var nodeCurry = string.Format("{0}{1}.{2}", apiparam.RootNode, "[" + i + "]", apiparam.CurrencyNode);
                        var nodeRate = string.Format("$.{0}{1}.{2}", apiparam.RootNode, "[" + i + "]", apiparam.RatioNode);
                        if (myJObject.SelectToken(nodeCurry) == null || myJObject.SelectToken(nodeRate) == null)
                        {
                            throw new PXException("Please use valid API.");
                        }
                        curryCode = myJObject.SelectToken(nodeCurry).HasValues ? "" : myJObject.SelectToken(nodeCurry).Value<string>();
                        var tokenRatio = myJObject.SelectToken(nodeRate);
                        curryRate = tokenRatio.HasValues ? 0 : tokenRatio.Value<decimal?>();
                    }
                    else
                    {
                        var exRate = myList[i] as dynamic;
                        curryCode = exRate.Name;
                        curryRate = exRate.Value;
                    }
                    apiPairs[curryCode] = curryRate.HasValue ? curryRate.Value : 0;
                }

                return apiPairs;
            }
            catch (Exception ex)
            {
                PXTrace.WriteError(ex);
                throw ex;
            }



        }

        public List<string> PopulateNodes(string json)
        {
            dynamic config = JsonConvert.DeserializeObject<ExpandoObject>(json, new ExpandoObjectConverter());
            Dictionary<string, object> currdictionary = ConvertDynamicToDictonary(config);
            List<NodeItem> hierarchyItems = new List<NodeItem>();
            foreach (var item in currdictionary)
            {
                var key = item.Key;
                var items = item.Value;
                bool isRoot = items is IEnumerable<object>;
                hierarchyItems.Add(new NodeItem { Name = key, IsRoot = isRoot });
                if (items is IEnumerable<object>)
                {
                    var ditems = (items as IEnumerable<object>).ToList()[0] as Dictionary<string, object>;
                    foreach (var eitem in ditems)
                    {
                        if (eitem.Value is Dictionary<string, object>)
                        {
                            key += "." + eitem.Key;
                            hierarchyItems.Add(new NodeItem { Name = key });
                            var eitems = (eitem.Value as Dictionary<string, object>);
                            LoadHierarchyItems(eitems, key, hierarchyItems);

                        }
                        else
                        {
                            key += "." + eitem.Key;
                            hierarchyItems.Add(new NodeItem { Name = key });
                            key = key.Replace("." + eitem.Key, "");
                        }
                    }
                }
                if (items is Dictionary<string, object>)
                {
                    var ditems = items as Dictionary<string, object>;
                    foreach (var eitem in ditems)
                    {
                        isRoot = eitem.Value is IEnumerable<object>;
                        if (eitem.Value is IEnumerable<object>)
                        {
                            var fitems = (eitem.Value as IEnumerable<object>).ToList()[0] as Dictionary<string, object>;
                            key += "." + eitem.Key;
                            hierarchyItems.Add(new NodeItem { Name = key, IsRoot = isRoot });
                            LoadHierarchyItems(fitems, key, hierarchyItems);

                        }
                        else
                        {
                            key += "." + eitem.Key;
                            hierarchyItems.Add(new NodeItem { Name = key });
                            key = key.Replace("." + eitem.Key, "");
                        }
                    }
                }
            }
            var rootNode = hierarchyItems.FirstOrDefault(f => f.IsRoot).Name;
            List<string> newitems = new List<string>();
            newitems.Add(rootNode);
            foreach (var item in hierarchyItems)
            {
                if (item.Name != rootNode)
                {
                    var replacedItem = item.Name.Replace(rootNode + ".", "");
                    newitems.Add(replacedItem);
                }
            }

            return newitems;
        }

        private static void LoadHierarchyItems(Dictionary<string, object> eitems, string key, List<NodeItem> hierarchyItems)
        {
            foreach (var eiItem in eitems)
            {

                if ((eiItem.Value is Dictionary<string, object>))
                {
                    key += "." + eiItem.Key;
                    hierarchyItems.Add(new NodeItem { Name = key });
                    LoadHierarchyItems((eiItem.Value as Dictionary<string, object>), key, hierarchyItems);
                }
                else
                {
                    key += "." + eiItem.Key;
                    hierarchyItems.Add(new NodeItem { Name = key });
                    key = key.Replace("." + eiItem.Key, "");
                }
            }
        }
        private Dictionary<string, object> ConvertDynamicToDictonary(IDictionary<string, object> value)
        {
            return value.ToDictionary(
                p => p.Key,
                p =>
                {
                    // if it's another IDict (might be a ExpandoObject or could also be an actual Dict containing ExpandoObjects) just go through it recursively
                    if (p.Value is IDictionary<string, object> dict)
                    {
                        return ConvertDynamicToDictonary(dict);
                    }

                    // if it's an IEnumerable, it might have ExpandoObjects inside, so check for that
                    if (p.Value is IEnumerable<object> list)
                    {
                        if (list.Any(o => o is ExpandoObject))
                        {
                            // if it does contain ExpandoObjects, take all of those and also go through them recursively
                            return list
                                        .Where(o => o is ExpandoObject)
                                        .Select(o => ConvertDynamicToDictonary((ExpandoObject)o));
                        }
                    }

                    // neither an IDict nor an IEnumerable -> it's probably fine to just return the value it has
                    return p.Value;
                }
            );
        }
    }

    public class ApiParam
    {
        public string APIUrl { get; set; }
        public string AuthType { get; set; }
        public string AuthToken { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ContentType { get; set; }
        public string ResponseType { get; set; }
        public string RootNode { get; set; }
        public string CurrencyNode { get; set; }
        public string RatioNode { get; set; }
        public string AcceptHeader { get; set; }
    }

    public class NodeItem
    {
        public string Name { get; set; }
        public bool IsRoot { get; set; }
    }
}
