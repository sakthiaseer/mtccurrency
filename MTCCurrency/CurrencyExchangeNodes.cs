using System;
using PX.Data;
using PX.Data.ReferentialIntegrity.Attributes;

namespace MTCCurrency
{
    [Serializable]
    [PXCacheName("CurrencyExchangeNodes")]
    public class CurrencyExchangeNodes : IBqlTable
    {
        #region CurrencyExchangeNodeID
        [PXDBIdentity(IsKey = true)]
        [PXReferentialIntegrityCheck]
        [PXUIField(DisplayName = "Currency Exchange Node ID", Visible = false)]
        public virtual int? CurrencyExchangeNodeID { get; set; }
        public abstract class currencyExchangeNodeID : PX.Data.BQL.BqlInt.Field<currencyExchangeNodeID> { }
        #endregion

        #region NodeName
        [PXDBString(1000, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Node Name")]
        public virtual string NodeName { get; set; }
        public abstract class nodeName : PX.Data.BQL.BqlString.Field<nodeName> { }
        #endregion

        #region ExchangeNoteID
        [PXDBGuid()]
        [PXUIField(DisplayName = "Exchange Note ID")]
        public virtual Guid? ExchangeNoteID { get; set; }
        public abstract class exchangeNoteID : PX.Data.BQL.BqlGuid.Field<exchangeNoteID> { }
        #endregion

        #region CreatedByID
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID { get; set; }
        public abstract class createdByID : PX.Data.BQL.BqlGuid.Field<createdByID> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region CreatedDateTime
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime { get; set; }
        public abstract class createdDateTime : PX.Data.BQL.BqlDateTime.Field<createdDateTime> { }
        #endregion

        #region LastModifiedByID
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID { get; set; }
        public abstract class lastModifiedByID : PX.Data.BQL.BqlGuid.Field<lastModifiedByID> { }
        #endregion

        #region LastModifiedByScreenID
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID { get; set; }
        public abstract class lastModifiedByScreenID : PX.Data.BQL.BqlString.Field<lastModifiedByScreenID> { }
        #endregion

        #region LastModifiedDateTime
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime { get; set; }
        public abstract class lastModifiedDateTime : PX.Data.BQL.BqlDateTime.Field<lastModifiedDateTime> { }
        #endregion

        #region Tstamp
        [PXDBTimestamp()]
        [PXUIField(DisplayName = "Tstamp")]
        public virtual byte[] Tstamp { get; set; }
        public abstract class tstamp : PX.Data.BQL.BqlByteArray.Field<tstamp> { }
        #endregion
    }
}