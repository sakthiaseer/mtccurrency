using System;
using PX.Data;
using PX.Data.ReferentialIntegrity.Attributes;

namespace MTCCurrency
{
    [Serializable]
    [PXCacheName("CurrencyExchangeSetup")]
    public class CurrencyExchangeSetup : IBqlTable
    {
        #region CurrencyExchangeSetupID
        [PXDBIdentity]
        [PXReferentialIntegrityCheck]
        [PXUIField(DisplayName = "Exchange Setup Id")]
        public virtual int? CurrencyExchangeSetupID { get; set; }
        public abstract class currencyExchangeSetupID : PX.Data.BQL.BqlInt.Field<currencyExchangeSetupID> { }
        #endregion

        #region OpenAPIName
        [PXDBString(100, IsUnicode = true, IsKey = true, InputMask = "")]
        [PXUIField(DisplayName = "Open APIName", Required = true, Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<openAPIName>), typeof(openAPIName), typeof(authType), typeof(responseType), SubstituteKey = typeof(openAPIName), Filterable = true)]
        public virtual string OpenAPIName { get; set; }
        public abstract class openAPIName : PX.Data.BQL.BqlString.Field<openAPIName> { }
        #endregion

        #region Openapiurl
        [PXDBString(1000, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Openapiurl")]
        public virtual string Openapiurl { get; set; }
        public abstract class openapiurl : PX.Data.BQL.BqlString.Field<openapiurl> { }
        #endregion

        #region AuthType
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Auth Type")]
        [PXStringList(new string[] { "AuthCode", "AuthCredential", "AuthToken", "Open" }, new string[] { "Auth Code", "Auth Credential", "Auth Token", "Open" })]
        [PXDefault("AuthCode")]
        public virtual string AuthType { get; set; }
        public abstract class authType : PX.Data.BQL.BqlString.Field<authType> { }
        #endregion

        #region ResponseType
        [PXDBString(10, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Response Type")]
        [PXStringList(new string[] { "XML", "JSON" }, new string[] { "XML", "JSON" })]
        [PXDefault("XML")]
        public virtual string ResponseType { get; set; }
        public abstract class responseType : PX.Data.BQL.BqlString.Field<responseType> { }
        #endregion

        #region RootNode
        [PXDBString(100, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Root Node", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<CurrencyExchangeNodes.nodeName, Where<CurrencyExchangeNodes.exchangeNoteID, Equal<Current<noteid>>>>), typeof(CurrencyExchangeNodes.nodeName), SubstituteKey = typeof(CurrencyExchangeNodes.nodeName), Filterable = true)]
        public virtual string RootNode { get; set; }
        public abstract class rootNode : PX.Data.BQL.BqlString.Field<rootNode> { }
        #endregion

        #region CurrencyNode
        [PXDBString(100, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Currency Node", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<CurrencyExchangeNodes.nodeName, Where<CurrencyExchangeNodes.exchangeNoteID, Equal<Current<noteid>>>>), typeof(CurrencyExchangeNodes.nodeName), SubstituteKey = typeof(CurrencyExchangeNodes.nodeName), Filterable = true)]

        public virtual string CurrencyNode { get; set; }
        public abstract class currencyNode : PX.Data.BQL.BqlString.Field<currencyNode> { }
        #endregion

        #region RateNode
        [PXDBString(100, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Rate Node", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<CurrencyExchangeNodes.nodeName, Where<CurrencyExchangeNodes.exchangeNoteID, Equal<Current<noteid>>>>), typeof(CurrencyExchangeNodes.nodeName), SubstituteKey = typeof(CurrencyExchangeNodes.nodeName), Filterable = true)]

        public virtual string RateNode { get; set; }
        public abstract class rateNode : PX.Data.BQL.BqlString.Field<rateNode> { }
        #endregion

        #region APIStatus
        [PXDBBool()]
        [PXUIField(DisplayName = "API Connected")]
        [PXDefault(false)]
        public virtual bool? APIStatus { get; set; }
        public abstract class aPIStatus : PX.Data.BQL.BqlBool.Field<aPIStatus> { }
        #endregion

        #region UserName
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "User Name", Enabled = false)]
        public virtual string UserName { get; set; }
        public abstract class userName : PX.Data.BQL.BqlString.Field<userName> { }
        #endregion

        #region Password
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Password", Enabled = false)]
        public virtual string Password { get; set; }
        public abstract class password : PX.Data.BQL.BqlString.Field<password> { }
        #endregion

        #region Authcode
        [PXDBString(100, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Auth Code", Enabled = false)]
        public virtual string AuthCode { get; set; }
        public abstract class authCode : PX.Data.BQL.BqlString.Field<authCode> { }
        #endregion

        #region AcceptHeader
        [PXDBString(100, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Accept Header")]
        public virtual string AcceptHeader { get; set; }
        public abstract class acceptHeader : PX.Data.BQL.BqlString.Field<acceptHeader> { }
        #endregion

        #region APIContent
        [PXDBString(-1, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "API Response")]
        public virtual string APIContent { get; set; }
        public abstract class apiContent : PX.Data.BQL.BqlString.Field<apiContent> { }
        #endregion

        #region CreatedByID
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID { get; set; }
        public abstract class createdByID : PX.Data.BQL.BqlGuid.Field<createdByID> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region CreatedDateTime
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime { get; set; }
        public abstract class createdDateTime : PX.Data.BQL.BqlDateTime.Field<createdDateTime> { }
        #endregion

        #region LastModifiedByID
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID { get; set; }
        public abstract class lastModifiedByID : PX.Data.BQL.BqlGuid.Field<lastModifiedByID> { }
        #endregion

        #region LastModifiedByScreenID
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID { get; set; }
        public abstract class lastModifiedByScreenID : PX.Data.BQL.BqlString.Field<lastModifiedByScreenID> { }
        #endregion

        #region LastModifiedDateTime
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime { get; set; }
        public abstract class lastModifiedDateTime : PX.Data.BQL.BqlDateTime.Field<lastModifiedDateTime> { }
        #endregion

        #region Tstamp
        [PXDBTimestamp()]
        [PXUIField(DisplayName = "Tstamp")]
        public virtual byte[] Tstamp { get; set; }
        public abstract class tstamp : PX.Data.BQL.BqlByteArray.Field<tstamp> { }
        #endregion

        #region Noteid
        [PXNote()]
        public virtual Guid? Noteid { get; set; }
        public abstract class noteid : PX.Data.BQL.BqlGuid.Field<noteid> { }
        #endregion

        #region RootNode
        [PXDBString(100, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Base Currency")]
        public virtual string BaseCurrency { get; set; }
        public abstract class baseCurrency : PX.Data.BQL.BqlString.Field<baseCurrency> { }
        #endregion
    }
}