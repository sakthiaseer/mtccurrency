using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.CM;
using PX.Objects;
using System.Collections.Generic;
using System;
using MTCCurrency;

namespace PX.Objects.CM
{
    public class CurrencyRateTypeExt : PXCacheExtension<PX.Objects.CM.CurrencyRateType>
    {
        #region UsrExchangeSetup
        [PXDBGuid]
        [PXUIField(DisplayName = "Exchange Setup", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<CurrencyExchangeSetup.noteid>), typeof(CurrencyExchangeSetup.openAPIName), typeof(CurrencyExchangeSetup.authType), typeof(CurrencyExchangeSetup.responseType), SubstituteKey = typeof(CurrencyExchangeSetup.openAPIName), Filterable = true)]
        public virtual Guid? UsrExchangeSetup { get; set; }
        public abstract class usrExchangeSetup : PX.Data.BQL.BqlGuid.Field<usrExchangeSetup> { }
        #endregion
    }
}