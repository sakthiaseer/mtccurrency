using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PX.Data;

namespace MTCCurrency
{
    public class CExchangeSetupMaint : PXGraph<CExchangeSetupMaint, CurrencyExchangeSetup>
    {
        List<string> nodeItems = new List<string>();
        public PXSelect<CurrencyExchangeSetup> MasterView;
        public PXSelect<CurrencySetupLine,
                 Where<CurrencySetupLine.exchangeNoteID, Equal<Current<CurrencyExchangeSetup.noteid>>>>
               DetailsView;

        public PXSelect<CurrencyExchangeNodes,
                 Where<CurrencyExchangeNodes.exchangeNoteID, Equal<Current<CurrencyExchangeSetup.noteid>>>>
               NodeItems;

        public PXAction<CurrencyExchangeSetup> ConnectAPI;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Connect API", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        protected IEnumerable connectAPI(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    NovatoService novatoService = new NovatoService();
                    ApiParam apiparam = new ApiParam();
                    var exportSetup = this.MasterView.Current;
                    apiparam.APIUrl = exportSetup.Openapiurl;
                    apiparam.AuthType = exportSetup.AuthType;
                    apiparam.AuthToken = exportSetup.AuthCode;
                    apiparam.UserName = exportSetup.UserName;
                    apiparam.Password = exportSetup.Password;
                    apiparam.RootNode = exportSetup.RootNode;
                    apiparam.CurrencyNode = exportSetup.CurrencyNode;
                    apiparam.ResponseType = exportSetup.ResponseType;
                    apiparam.RatioNode = exportSetup.RateNode;
                    apiparam.AcceptHeader = exportSetup.AcceptHeader;
                    if (exportSetup.ResponseType == "XML")
                    {
                        apiparam.ContentType = "application/xml";
                    }
                    if (exportSetup.ResponseType == "JSON")
                    {
                        apiparam.ContentType = "application/json";
                    }
                    Dictionary<string, decimal> apiData = new Dictionary<string, decimal>();
                    var apiTask = Task.Run(async () =>
                     {
                         apiData = await novatoService.GetApiData(apiparam);
                     });
                    apiTask.Wait();
                    if (apiData.Count == 0)
                    {
                        throw new PXException("Please use valid API.");
                    }
                    foreach (var item in apiData)
                    {
                        this.DetailsView.Insert(new CurrencySetupLine
                        {
                            BaseCurrency = exportSetup.BaseCurrency,
                            CurrencyCode = item.Key,
                            Ratio = item.Value,
                            LastSyncDate = DateTime.Now
                        });
                    }
                    this.MasterView.Current.APIStatus = true;
                    this.MasterView.Update(this.MasterView.Current);
                    this.Save.Press();

                }
                catch (Exception ex)
                {
                    PXTrace.WriteError(ex);
                    throw ex;
                }
            });

            return adapter.Get();
        }

        public PXAction<CurrencyExchangeSetup> ShowAPIContent;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Verify API", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        protected IEnumerable showAPIContent(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    NovatoService novatoService = new NovatoService();
                    ApiParam apiparam = new ApiParam();
                    var exportSetup = this.MasterView.Current;
                    apiparam.APIUrl = exportSetup.Openapiurl;
                    apiparam.AuthType = exportSetup.AuthType;
                    apiparam.AuthToken = exportSetup.AuthCode;
                    apiparam.UserName = exportSetup.UserName;
                    apiparam.Password = exportSetup.Password;
                    apiparam.RootNode = exportSetup.RootNode;
                    apiparam.CurrencyNode = exportSetup.CurrencyNode;
                    apiparam.ResponseType = exportSetup.ResponseType;
                    apiparam.RatioNode = exportSetup.RateNode;
                    apiparam.AcceptHeader = exportSetup.AcceptHeader;
                    if (exportSetup.ResponseType == "XML")
                    {
                        apiparam.ContentType = "application/xml";
                    }
                    if (exportSetup.ResponseType == "JSON")
                    {
                        apiparam.ContentType = "application/json";
                    }
                    string apiData = string.Empty;
                    var apiTask = Task.Run(async () =>
                    {
                        apiData = await novatoService.GetApiContent(apiparam);
                    });
                    apiTask.Wait();

                    if (apiparam.ResponseType == "XML")
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(apiData);
                        apiData = JsonConvert.SerializeXmlNode(doc);
                    }
                    nodeItems = novatoService.PopulateNodes(apiData);

                    exportSetup.APIContent = apiData;
                    this.MasterView.Cache.Update(exportSetup);
                    this.MasterView.Update(exportSetup);

                    var existingNodeItems = this.NodeItems.Select().FirstTableItems.ToList();

                    existingNodeItems.ForEach(e =>
                    {
                        this.NodeItems.Delete(e);
                    });
                    this.Save.Press();

                    List<CurrencyExchangeNodes> currencyExchangeNodeItems = new List<CurrencyExchangeNodes>();
                    nodeItems.ForEach(n =>
                    {
                        CurrencyExchangeNodes currencyExchangeNodes = new CurrencyExchangeNodes();
                        currencyExchangeNodes.NodeName = n;
                        currencyExchangeNodes.ExchangeNoteID = exportSetup.Noteid;
                        this.NodeItems.Insert(currencyExchangeNodes);
                    });

                    this.Save.Press();
                    this.MasterView.Cache.Clear();
                    this.MasterView.Cache.ClearQueryCache();
                }
                catch (Exception ex)
                {
                    PXTrace.WriteError(ex);
                    throw ex;
                }
            });

            return adapter.Get();
        }

        #region Event Handlers

        protected void CurrencyExchangeSetup_AuthType_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e, PXFieldUpdated InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null)
                InvokeBaseHandler(cache, e);
            var row = (CurrencyExchangeSetup)e.Row;

        }

        protected virtual void CurrencyExchangeSetup_RowSelected(PXCache sender, PXRowSelectedEventArgs e, PXRowSelected BaseEvent)
        {

            var rows = e.Row as CurrencyExchangeSetup;
            if (rows == null)
                return;

            if (rows.APIStatus == true)
            {
                this.ConnectAPI.SetEnabled(false);
            }

            if (rows.AuthType == "AuthCode" || rows.AuthType == "AuthToken"|| rows.AuthType == "Authkey")
            {
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.authCode>(sender, rows, true);
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.userName>(sender, rows, false);
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.password>(sender, rows, false);
            }
            if (rows.AuthType == "AuthCredential")
            {
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.userName>(sender, rows, true);
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.password>(sender, rows, true);
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.authCode>(sender, rows, false);
            }
            if (rows.AuthType == "Open")
            {
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.userName>(sender, rows, false);
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.password>(sender, rows, false);
                PXUIFieldAttribute.SetEnabled<CurrencyExchangeSetup.authCode>(sender, rows, false);
            }


        }

        #endregion
    }
}