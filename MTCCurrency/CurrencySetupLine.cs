using System;
using PX.Data;
using PX.Data.ReferentialIntegrity.Attributes;

namespace MTCCurrency
{
    [Serializable]
    [PXCacheName("CurrencySetupLine")]
    public class CurrencySetupLine : IBqlTable, ISortOrder
    {
        #region LineNbr
        [PXUIField(DisplayName = "Line Nbr", Visible = false)]
        [PXDBIdentity(IsKey = true)]
        [PXReferentialIntegrityCheck]
        public virtual int? LineNbr { get; set; }
        public abstract class lineNbr : PX.Data.BQL.BqlInt.Field<lineNbr> { }
        #endregion

        #region SortOrder
        [PXDBInt()]
        [PXUIField(DisplayName = "Sort Order", Visible = false, Enabled = false)]
        public virtual int? SortOrder { get; set; }
        public abstract class sortOrder : IBqlField { }
        #endregion

        #region BaseCurrency
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Base Currency")]
        public virtual string BaseCurrency { get; set; }
        public abstract class baseCurrency : PX.Data.BQL.BqlString.Field<baseCurrency> { }
        #endregion


        #region CurrencyCode
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Currency Code")]
        public virtual string CurrencyCode { get; set; }
        public abstract class currencyCode : PX.Data.BQL.BqlString.Field<currencyCode> { }
        #endregion

        #region Ratio
        [PXDBDecimal()]
        [PXUIField(DisplayName = "Ratio")]
        public virtual Decimal? Ratio { get; set; }
        public abstract class ratio : PX.Data.BQL.BqlDecimal.Field<ratio> { }
        #endregion

        #region LastSyncDate
        [PXDBDate()]
        [PXUIField(DisplayName = "Last Sync Date")]
        public virtual DateTime? LastSyncDate { get; set; }
        public abstract class lastSyncDate : PX.Data.BQL.BqlDateTime.Field<lastSyncDate> { }
        #endregion

        #region CreatedByID
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID { get; set; }
        public abstract class createdByID : PX.Data.BQL.BqlGuid.Field<createdByID> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region CreatedDateTime
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime { get; set; }
        public abstract class createdDateTime : PX.Data.BQL.BqlDateTime.Field<createdDateTime> { }
        #endregion

        #region LastModifiedByID
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID { get; set; }
        public abstract class lastModifiedByID : PX.Data.BQL.BqlGuid.Field<lastModifiedByID> { }
        #endregion

        #region LastModifiedByScreenID
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID { get; set; }
        public abstract class lastModifiedByScreenID : PX.Data.BQL.BqlString.Field<lastModifiedByScreenID> { }
        #endregion

        #region LastModifiedDateTime
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime { get; set; }
        public abstract class lastModifiedDateTime : PX.Data.BQL.BqlDateTime.Field<lastModifiedDateTime> { }
        #endregion

        #region Tstamp
        [PXDBTimestamp()]
        [PXUIField(DisplayName = "Tstamp")]
        public virtual byte[] Tstamp { get; set; }
        public abstract class tstamp : PX.Data.BQL.BqlByteArray.Field<tstamp> { }
        #endregion

        #region ExchangeNoteID
        [PXDBGuid()]
        [PXUIField(DisplayName = "Exchange Note ID")]
        [PXDefault(typeof(CurrencyExchangeSetup.noteid))]
        public virtual Guid? ExchangeNoteID { get; set; }
        public abstract class exchangeNoteID : PX.Data.BQL.BqlGuid.Field<exchangeNoteID> { }
        #endregion
    }
}