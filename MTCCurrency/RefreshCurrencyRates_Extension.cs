﻿using PX.Data;
using PX.Objects.CS;
using PX.Objects.GL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using PX.Objects;
using PX.Objects.CM;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace MTCCurrency
{
    public class RefreshCurrencyRates_Extension : PXGraphExtension<RefreshCurrencyRates>
    {
        #region Event Handlers
        public delegate Dictionary<String, Decimal> GetRatesFromServiceDelegate(RefreshFilter filter, List<RefreshRate> list, DateTime date);
        [PXOverride]
        public Dictionary<String, Decimal> GetRatesFromService(RefreshFilter filter, List<RefreshRate> list, DateTime date, GetRatesFromServiceDelegate baseMethod)
        {

            var currencyRate = PXSelect<CurrencyRateType, Where<CurrencyRateType.curyRateTypeID,
               Equal<Required<CurrencyRateType.curyRateTypeID>>>>
               .Select(Base, filter.CuryRateTypeID).FirstTableItems.FirstOrDefault();
            var rowExt = currencyRate.GetExtension<CurrencyRateTypeExt>();
            if (rowExt != null)
            {
                var exchangeSetupId = rowExt.UsrExchangeSetup;

                var exportSetup = PXSelect<CurrencyExchangeSetup, Where<CurrencyExchangeSetup.noteid,
                Equal<Required<CurrencyRateTypeExt.usrExchangeSetup>>>>
                .Select(Base, exchangeSetupId).FirstTableItems.FirstOrDefault();
                if (exportSetup != null && exportSetup.APIStatus == true)
                {
                    try
                    {
                        NovatoService novatoService = new NovatoService();
                        ApiParam apiparam = new ApiParam();
                        apiparam.APIUrl = exportSetup.Openapiurl;
                        apiparam.AuthType = exportSetup.AuthType;
                        apiparam.AuthToken = exportSetup.AuthCode;
                        apiparam.UserName = exportSetup.UserName;
                        apiparam.Password = exportSetup.Password;
                        apiparam.RootNode = exportSetup.RootNode;
                        apiparam.CurrencyNode = exportSetup.CurrencyNode;
                        apiparam.ResponseType = exportSetup.ResponseType;
                        apiparam.RatioNode = exportSetup.RateNode;
                        if (exportSetup.ResponseType == "XML")
                        {
                            apiparam.ContentType = "application/xml";
                        }
                        if (exportSetup.ResponseType == "JSON")
                        {
                            apiparam.ContentType = "application/json";
                        }
                        Dictionary<string, decimal> apiData = new Dictionary<string, decimal>();
                        var apiTask = Task.Run(async () =>
                        {
                            apiData = await novatoService.GetApiData(apiparam);
                        });
                        apiTask.Wait();
                        return apiData;
                    }
                    catch (Exception ex)
                    {
                        PXTrace.WriteError(ex);
                        throw ex;
                    }
                }
                else
                {
                    throw new PXException("Please use Connected API.");
                }
            }

            return baseMethod(filter, list, date);
        }
        #endregion
    }
}
